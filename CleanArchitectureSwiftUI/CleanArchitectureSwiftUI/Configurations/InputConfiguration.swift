//
//  InputConfiguration.swift
//  CleanArchitectureSwiftUI
//
//  Created by Marco Feltmann on 30.11.20.
//

import Foundation

struct InputConfiguration : TimerConfigurable {
    var id = UUID().uuidString
    var ringingExecution: RingingExecution = .immediate
}
