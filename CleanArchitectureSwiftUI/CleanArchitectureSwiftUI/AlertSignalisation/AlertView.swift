//
//  AlertView.swift
//  CleanArchitectureSwiftUI
//
//  Created by Marco Feltmann on 28.11.20.
//

import SwiftUI
import AVFoundation

struct AlertView : View {
    @ObservedObject var alertPublisher : AlertTimerPublisher
    
    @State var latestRingIdentifer = ""
    @State var numberOfSleeps = 0
    
    var body: some View {
        if alertPublisher.ringingIdentifier != latestRingIdentifer {
            VStack {
                HStack {
                    Image(systemName: "alarm.fill")
                        .resizable()
                        .frame(width: 20, height: 20, alignment: .center)

                    if numberOfSleeps == 0 {
                        Text("Ring Ring Ring")
                            .font(.largeTitle)
                    } else {
                        Text("Erinnerung #\(numberOfSleeps)")
                            .font(.largeTitle)
                    }
                    Image(systemName: "alarm.fill")
                        .resizable()
                        .frame(width: 20, height: 20, alignment: .center)
                }
                Button("Ruhe!") {
                    self.latestRingIdentifer = alertPublisher.ringingIdentifier
                    self.numberOfSleeps = 0
                    alertPublisher.triggerNewAlert(id: "\(latestRingIdentifer)#Silenced", isImmediate: false)
                }
                Button("Später…") {
                    self.latestRingIdentifer = alertPublisher.ringingIdentifier
                    DispatchQueue.init(label: "WeckerSleepQueue", qos: .background).async {
                        numberOfSleeps += 1
                        sleep(UInt32(numberOfSleeps))
                        alertPublisher.triggerNewAlert(id:"\(latestRingIdentifer)+")
                    }
                }
            }
        }
        else {
            Image(systemName: "deskclock")
                .resizable()
                .frame(width: 20, height: 20, alignment: .center)
        }
    }
}

#if DEBUG
struct AlertView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            AlertView(alertPublisher: AlertTimerPublisher(TimerLogic<InputConfiguration>()))
                .previewDisplayName("Not Ringing")
            
            AlertView(alertPublisher: RingingAlertTimerPublisher(TimerLogic<InputConfiguration>()))
                .previewDisplayName("Alert Was Ringing")
            
        }
    }
}

class RingingAlertTimerPublisher : AlertTimerPublisher {
    override var ringingIdentifier: String {
        get { "RingRingRing" }
        set { }
    }
}
#endif
