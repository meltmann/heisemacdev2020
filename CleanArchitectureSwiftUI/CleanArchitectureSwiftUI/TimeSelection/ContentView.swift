//
//  ContentView.swift
//  CleanArchitectureSwiftUI
//
//  Created by Marco Feltmann on 27.11.20.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var timeTrigger : AlertTimerPublisher
    var body: some View {
        VStack {
            Text("Hier könnte IHRE Weckzeit stehen!")
                .font(.largeTitle)
            Button("Test…") {
                let id = UUID().uuidString
                timeTrigger.triggerNewAlert(id: id, isImmediate: true)
                timeTrigger.triggerNewAlert(id: id, isImmediate: false)
            }
            .padding(.bottom)
            Divider()
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
