//
//  TimerLogic.swift
//  CleanArchitectureSwiftUI
//
//  Created by Marco Feltmann on 28.11.20.
//

import Combine


public struct TimerLogic<InputConfiguration> where InputConfiguration : TimerConfigurable {
    public func ring(with config: InputConfiguration) {
        switch config.ringingExecution {
        case .immediate:
            self.immediateRingingPublisher.send(ImmediateRinger(id: config.id))
            
        case .deferred:
            self.deferredRingingPublisher.send(DeferredRinger(id: config.id))
        }
    }
    
    public let immediateRingingPublisher = PassthroughSubject<ImmediateRinger, Never>()
    public let deferredRingingPublisher = PassthroughSubject<DeferredRinger, Never>()
}

public protocol TimerConfigurable : Identifiable {
    var id : String { get }
    var ringingExecution : RingingExecution { get }
}

public enum RingingExecution {
    case immediate
    case deferred
}

public struct ImmediateRinger : Identifiable {
    public let id: String
}

public struct DeferredRinger : Identifiable {
    public let id: String
}
