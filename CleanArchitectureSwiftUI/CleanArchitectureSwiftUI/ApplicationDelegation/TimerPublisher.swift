//
//  TimerPublisher.swift
//  CleanArchitectureSwiftUI
//
//  Created by Marco Feltmann on 28.11.20.
//

import SwiftUI
import Combine

public class AlertTimerPublisher : ObservableObject {
    @Published var ringingIdentifier = ""
    
    let timerLogic : TimerLogic<InputConfiguration>
    
    private var registerIdentifierCancellable : AnyCancellable?
    
    init(_ logicToRegisterTo: TimerLogic<InputConfiguration>) {
        self.timerLogic = logicToRegisterTo
        
        print("Registering AlertTimerPublisher:ObservableObject to TimerLogic object")
        
        self.registerIdentifierCancellable = self.timerLogic.immediateRingingPublisher
            .print()
            .sink { ringOutput in
                print("Receiver = AlertTimerPublisher:ObservableObject")
                self.ringingIdentifier = ringOutput.id
            }
    }
    
    public func triggerNewAlert(id:String?, isImmediate:Bool = true)->() {
        var configuration = InputConfiguration()
        if let givenId = id {
            configuration.id = givenId
        }
        if !isImmediate {
            configuration.ringingExecution = RingingExecution.deferred
        }
        
        DispatchQueue.main.async {
            self.timerLogic.ring(with: configuration)
            // TimerLogicImpl.ring oder so…
        }
    }
}
