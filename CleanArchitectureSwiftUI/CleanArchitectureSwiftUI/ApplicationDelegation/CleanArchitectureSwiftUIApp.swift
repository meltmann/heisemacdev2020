//
//  CleanArchitectureSwiftUIApp.swift
//  CleanArchitectureSwiftUI
//
//  Created by Marco Feltmann on 27.11.20.
//

import SwiftUI
import Combine

@main
struct CleanArchitectureSwiftUIApp: App {
    let sharedTimerLogic = TimerLogic<InputConfiguration>()
    let timer:AlertTimerPublisher
    
    init() {
        timer = AlertTimerPublisher(self.sharedTimerLogic)
    }
    
    var body: some Scene {
        WindowGroup {
            VStack {
                AlertView(alertPublisher: self.timer)
                    .padding()
                Divider()
                ContentView()
                    .environmentObject(self.timer)
                    .padding()
                
                Log(timerLogic: self.sharedTimerLogic)
            }
            .frame(width: 800, height: 600, alignment: .center)
        }
    }
}

struct Log : View {
    var timerLogic : TimerLogic<InputConfiguration>
    
    @ObservedObject var logs:LogStore = LogStore.sharedInstance
    
    var immediateCancellable : AnyCancellable?
    var deferredCancellable : AnyCancellable?
    
    init(timerLogic: TimerLogic<InputConfiguration>) {
        self.timerLogic = timerLogic
        
        print("Registering Log:View to TimerLogic object")
        
        immediateCancellable = timerLogic.immediateRingingPublisher
            .print()
            .sink {
                print("Immediate Receiver = Log:View")
                LogStore.sharedInstance.observableDidChange("I:\($0.id)")
            }
        
        deferredCancellable = timerLogic.deferredRingingPublisher
            .print()
            .sink {
                print("Deferred Receiver = Log:View")
                LogStore.sharedInstance.observableDidChange("D:\($0.id)")
            }
    }
    var body : some View {
        List(logs.triggers) { triggerEntry in
            HStack {
                Text("\(triggerEntry.level)")
                    .font(.largeTitle)
                Divider()
                VStack {
                    Text(triggerEntry.message)
                        .font(.largeTitle)
                    Text("\(triggerEntry.date)")
                }
            }
        }
    }
}

class LogStore : ObservableObject {
    static var sharedInstance = LogStore()

    @Published var triggers:[LogEntry] = []

    private init() { }

    func observableDidChange(_ value:String) {
        triggers.insert(LogEntry(message: value), at: 0)
    }
}

struct LogEntry : Identifiable {
    let message : String
    let date:Date = Date()
    let id:UUID = UUID()
    let level:Int = 4
}
